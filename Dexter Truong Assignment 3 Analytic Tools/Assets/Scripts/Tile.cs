﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    //Calls GaneManager script
    private GameManager m_manager;

    //Gets the symbols from the GameManager script
    public GameManager.Symbols Symbol { get; private set; }

    //Transform variable for child gameobjects
    private Transform m_child;

    //Variable for colliders
    private Collider m_collider;

    //GetComponent for transforms of children gameobject and colliders
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //When the player presses on a tile, call the TileSelected function from the GameManager script
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Calls and initializrs the GameManager script and the Symbol Enum from the GameManager script
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //Function for revealing tiles
    public void Reveal()
    {
        //When a tile is being revealed, stop all other coroutines
        StopAllCoroutines();

        //Starts the Spin Enum and spins the tile 180 degrees
        StartCoroutine( Spin( 180, 0.8f ) );

        //Disables the collider for the revealed tile
        m_collider.enabled = false;
    }

    //Function for hiding tiles
    public void Hide()
    {
        //When a tile is being hidden, stop all other coroutines
        StopAllCoroutines();

        //Starts the Spin Enum that flips the tile around to hide it
        StartCoroutine( Spin( 0, 0.8f ) );

        //Enables the collider for the hidden tile
        m_collider.enabled = true;
    }

    //Function for spinning the tile
    private IEnumerator Spin( float target, float time )
    {
        //Timer variable
        float timer = 0;

        //Rotation variable  that gets the Euler Angles of children gameobject
        float startingRotation = m_child.eulerAngles.y;

        //Vector3 of the children gameobject
        Vector3 euler = m_child.eulerAngles;

        //Spins the tile for X amount of time depending on whether the tile is being hidden or revealed
        while ( timer < time )
        {
            //Adds time to timer
            timer += Time.deltaTime;

            //Lerps the angle rotation fo the tile
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        } 
        //The target of the angle rotation of the tile
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
