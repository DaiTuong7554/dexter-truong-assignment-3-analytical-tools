﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    //THe int variable for indicating what level the player is on
    public static int CurrentLevelNumber;

    //Amount of clicks the pl;ayer makes
    public int MouseClicksAmount;

    public int mismatches;

    public int matches;

    public int LevelsDone;

    public int CardFlipped;

    //The types of matching symbols that can appear in the game
    [System.Flags]
    public enum Symbols
    {
        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    //Serializable Inspector options for editing levels
    [System.Serializable]
    public struct LevelDescription
    {
        //Number of rows and columns of tiles in the level
        public int Rows, Columns;

        //The types of symbols used in the level
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    //Gameobject of the tiles
    public GameObject TilePrefab;

    //The space between instantiated tiles
    public float TileSpacing;

    //Array for the levels
    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    //Calls the Tile script to acquires the 2 matching tiles 
    private Tile m_tileOne, m_tileTwo;

    //Number of tiles remaining in the level 
    private int m_cardsRemaining;

    private void Start()
    {
        //Loads the currently set levle number
        LoadLevel(CurrentLevelNumber);
    }

    //Function for loading levels
    private void LoadLevel(int levelNumber)
    {
        //Loads the level's number and length
        LevelDescription level = Levels[levelNumber % Levels.Length];

        //Acquires the symbols used in the level
        List<Symbols> symbols = GetRequiredSymbols(level);

        //Number of rows needed to be in the level depending on the level
        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            //Adds tile spacing between each instantiated tile
            float yPosition = rowIndex * (1 + TileSpacing);

            //Number of columns needed to be in the level depending on the level
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {
                //Instantiates the tiles with the set number of rows and columns
                //Uses the set tile symbols
                //Randomizes the positions of the tile symbols in the parameter of the number of rows
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);
                symbols.RemoveAt(symbolIndex);
            }
        }

        //The camera
        SetupCamera(level);
    }

    //Acquires the needed symnols in the level
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        //List of symbols
        List<Symbols> symbols = new List<Symbols>();

        //Total number cards determined by the number of rows and columns in the level
        int cardTotal = level.Rows * level.Columns;

        //Array of the number of symbols
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            //Number of cards remaining
            m_cardsRemaining = cardTotal;

            //Creates an error if there is an odd number of cards inputed
            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
            }

            //Adds symbols to the level based on which ones are set 
            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //If there are no symbols set, throw an error
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }

            //If there are too many symbols set for the number of cards currently in the level, throw an error
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //Adds pairs of matching symbols dependent on how many symbols there are and card total
        {
            int repeatCount = (cardTotal / 2) - symbols.Count;

            //Adds copies of set symbols in another symbol copy array to create matching symbols 
            if (repeatCount > 0)
            {
                //List of symbols and a list of the symbol duplicates
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);
                List<Symbols> duplicateSymbols = new List<Symbols>();

                //For each symbol and it's dulpicate, randomize their index to randomize their positions on screen
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    symbolsCopy.RemoveAt(randomIndex);

                    //If the number of copy symbols is zero, appends symbols at the end of the symbol array
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }
                //Appends duplicate symbols at the end of the duplicate symbol array
                symbols.AddRange(duplicateSymbols);
            }
        }
        //Appends symbols at the end of the symbol array
        symbols.AddRange(symbols);

        return symbols;
    }

    //Vector of instantiated symbols
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        //From the array of symbols, get the value of the symbol Enum
        Array symbols = Enum.GetValues(typeof(Symbols));

        //For every symbol in the symbol index, get the values of the symbol Enum to be used as Vector2 coordinates
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                //Return the value of the symbol Enum as coordinates to the Vector2 
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }

        //If a symbol cannot be found, throw an error in the debug log
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //Sets up the camera based on the number of rows and columns
    //Makes sure all tiles are visible on camera at all times
    private void SetupCamera(LevelDescription level)
    {

        //MNakes the camera orthographic based on the number of rows, columns, and spacing of tiles
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //Function for selecting tiles
    public void TileSelected(Tile tile)
    {
        //if the first tile doesn't exist, call the first tile with the Tile script and reveal it to the player 
        if (m_tileOne == null)
        {
            CardFlipped++;
            m_tileOne = tile;
            m_tileOne.Reveal();


            //Analytics event for tracking whenever a card is flipped
            Analytics.CustomEvent("CardFlipped", new Dictionary<string, object>
                    {

            { "CardFlipped", CardFlipped },
        });
        }

        //if the second tile doesn't exist, call the first tile with the Tile script and reveal it to the player 
        else if (m_tileTwo == null)
        {
            CardFlipped++;
            m_tileTwo = tile;
            m_tileTwo.Reveal();

            //Analytics event for tracking whenever a card is flipped
            Analytics.CustomEvent("CardFlipped", new Dictionary<string, object>
                    {

            { "CardFlipped", CardFlipped },
        });

            //If the first and second tiles are matching, call the IEnumerator that hides tiles, which will destroy the matching tiles
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                StartCoroutine(WaitForHide(true, 1f));
            }

            //If the two tiles are not matching, do not call the IEnumerator that hides tiles
            else
            {
                StartCoroutine(WaitForHide(false, 1f));
            }
        }
    }

    //Function determines if the level is complete or not
    private void LevelComplete()
    {
        //Adds to current level number
        ++CurrentLevelNumber;

        //If the current number of levels is greater than the total length of levels, it's game over
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");

        }

        //If the current number of levels isn't greater than the length of levels, load the new scene of the level based on the build index
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //IEnumerator Function for how long tiles show up for
    //Also for destroying tiles on matching
    private IEnumerator WaitForHide(bool match, float time)
    {
        //Timer for the tiles showing up, set at 0
        float timer = 0;

        //If the time is greater than 0, break operation
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }

        //If the player gets a match, destroy both matched cards and remove them from the list
        if (match)
        {
            matches++;

            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;


            //Analytics event for tracking how many matches are made 
            Analytics.CustomEvent("Matches", new Dictionary<string, object>
                    {

            { "Matches", matches },

            

        });
        }

        //If the player doesn't get a match, hide the selected cards again
        else
        {
            mismatches++;

            m_tileOne.Hide();
            m_tileTwo.Hide();

            //Analytics event for tracking how many mismatches are made 
            Analytics.CustomEvent("Mismatches", new Dictionary<string, object>
                    {

            { "Mismatches", mismatches },


        });



        }
        m_tileOne = null;
        m_tileTwo = null;

        //If the number of cards left is zero, the level is complete
        if (m_cardsRemaining == 0)
        {

            LevelComplete();

            LevelsDone++;

            //Analytics event for tracking how many levels are completed
            Analytics.CustomEvent("LevelDone", new Dictionary<string, object>
                    {

            { "LevelDone", LevelsDone },


        });
        }
    }

    void Update()
    {

        //Analytics event for how many mouse clicks it takes to finish a level
        if (Input.GetMouseButtonDown(0))
        {

            print("yes");
            Analytics.CustomEvent("MouseClick", new Dictionary<string, object>
        {
            { "MouseClick", MouseClicksAmount },
        });
        }



    }



    }


